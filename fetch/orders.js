
fetch("https://api.ote.co.ke/api/v1/orders") 
.then((response) =>{
    response.json().then((items)=>{
        
        items = items["items"]
        console.log(items)
        let output = `

        <div class="table-responsive text-nowrap">
        <table class="table">
          <thead>
            <tr>             
              <th scope="col">Origin</th>
              <th scope="col">Origin Contact</th>
              <th scope="col">Destination</th>
              <th scope="col">Destination Contact</th>
              <th scope="col">Notes</th>
              <th scope="col">Status</th>
              <th scope="col">Actions</th>
              <th scope="col"></th>
            </tr>
          </thead>

        `;
        Object.keys(items).forEach(function(item){

            output +=`
            <tr>
           
                <td>${items[item]["origin"]}</td>
                <td>${items[item]["origin_contact"]}</td>
                <td>${items[item]["destination"]}</td>
                <td>${items[item]["destination_contact"]}</td>
                <td>${items[item]["notes"]}</td>
                <td>${items[item]["status"]}</td
                <td><button  class="btn btn-primary" onclick="edit(${items[item]["id"]})">Edit</button></td>
                <td><button  class="btn btn-danger" onclick="deletecustomer(${items[item]["id"]})">Delete</button></td>`
        })
            document.getElementById("orders").innerHTML = output + '</table>';
        })
        .catch(err => console.log(err));
})


// Delete Customers
function deletecustomer(id){
    console.log(id)
    let url = "https://api.ote.co.ke/api/v1/customers/"+id;
    if  (window.confirm("Are you sure, you want to delete?")){
        fetch(url, {
            method:"DELETE",
            headers:{"Content-Type":"application/json"}
        })
        .then((res)=> res.json())
        .then((res)=> {
            window.location.replace("customers.html")
        })
        
    }
}


